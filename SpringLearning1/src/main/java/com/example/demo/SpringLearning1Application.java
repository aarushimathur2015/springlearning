package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.example.demo.App;


@SpringBootApplication
public class SpringLearning1Application {

	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(SpringLearning1Application.class, args);
		App app = context.getBean(App.class);
		app.runApp();
		
		
	}

}
