package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class App {
	
	
	@Autowired
	Learning lrn;
	
	public void runApp()
	{
		System.out.println("Start the App!!");
		lrn.learn();
	}

}
